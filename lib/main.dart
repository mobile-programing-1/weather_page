//import 'dart:html';
import 'package:flutter/material.dart';
enum APP_THEME{LIGHT, DARK}
void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.black,
      ),
    );
  }
  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.black12,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.swap_horiz),
          onPressed: (){
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}


Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 250,

            child: Image.network(
              "https://scontent.fbkk30-1.fna.fbcdn.net/v/t39.30808-6/323632554_1226875591507540_151113587346283147_n.jpg?stp=cp6_dst-jpg&_nc_cat=102&ccb=1-7&_nc_sid=8631f5&_nc_eui2=AeHtvbP3xE7hiQ7apqAW_byQUEhh6UkAmlFQSGHpSQCaUQDyLfbRbmYc8c5_OToAcc_WnadKWJqCekVRglnYDTmQ&_nc_ohc=DlOtDaQhu8cAX-xwa8R&tn=L3KcCZDCsfw4Fv7g&_nc_ht=scontent.fbkk30-1.fna&oh=00_AfAKzIJmF15iJOHGV-CHc8m35_Ys4okxVWZDN7Za5n1Zng&oe=63B9EC4A",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "มีเมฆมากระหว่างเวลา 01:00 - 06:00 และคาดว่ามีแดด\nออกเป็นส่วนมากเวลา 11:00",
                    style: TextStyle(fontSize: 17),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(
                  iconTheme: IconThemeData(
                    color: Colors.blueAccent,
                  )
              ),
              child: profileActionItems(),
            ),
          ),
          Divider(
            color: Colors.white70,
          ),
          mobilePhoneListTile(),
          otherPhoneListTile(),
          Divider(
            color: Colors.grey,
          ),
          emailListTile(),
          Divider(
            color: Colors.grey,
          ),
          addressListTile(),
        ],
      ),
    ],
  );
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      Text("ตอนนี้"),
      IconButton(
        icon: Icon(
          Icons.wb_cloudy,
          //color: Colors.indigo.shade800, //เฉดสี
        ),
        onPressed: () {},
      ),
      Text("28°", style: TextStyle(fontSize: 17)),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      Text("23"),
      IconButton(
        icon: Icon(
          Icons.wb_cloudy,
          //color: Colors.indigo.shade800, //เฉดสี
        ),
        onPressed: () {},
      ),
      Text("24°", style: TextStyle(fontSize: 17)),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      Text("00"),
      IconButton(
        icon: Icon(
          Icons.wb_cloudy,
          //color: Colors.indigo.shade800, //เฉดสี
        ),
        onPressed: () {},
      ),
      Text("20°", style: TextStyle(fontSize: 17)),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      Text("01"),
      IconButton(
        icon: Icon(
          Icons.wb_cloudy,
          //color: Colors.indigo.shade800, //เฉดสี
        ),
        onPressed: () {},
      ),
      Text("20°", style: TextStyle(fontSize: 17)),
    ],
  );
}

Widget buildDirectionButton() {
  return Column(
    children: <Widget>[
      Text("02"),
      IconButton(
        icon: Icon(
          Icons.wb_cloudy,
          //color: Colors.indigo.shade800, //เฉดสี
        ),
        onPressed: () {},
      ),
      Text("20°", style: TextStyle(fontSize: 17)),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      Text("03"),
      IconButton(
        icon: Icon(
          Icons.wb_cloudy,
          //color: Colors.indigo.shade800, //เฉดสี
        ),
        onPressed: () {},
      ),
      Text("20°", style: TextStyle(fontSize: 17)),
    ],
  );
}

Widget mobilePhoneListTile(){
  return ListTile(
    leading: Text("28°", style: TextStyle(fontSize: 32)),
    title: Text("วันพรุ่งนี้"),
    subtitle: Text("มีแดดมาก"),
    trailing: IconButton(
      icon: Icon(Icons.sunny),
      color: Colors.deepOrange,
      onPressed: (){},
    ),
  );
}

Widget otherPhoneListTile(){
  return ListTile(
    leading: Text("26°", style: TextStyle(fontSize: 32)),
    title: Text("วันที่ 1 ม.ค. 2566"),
    subtitle: Text("มีฝนตกเล็กน้อย"),
    trailing: IconButton(
      icon: Icon(Icons.cloudy_snowing),
      color: Colors.blue,
      onPressed: (){},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Text("27°", style: TextStyle(fontSize: 32)),
    title: Text("วันที่ 2 ม.ค. 2566"),
    subtitle: Text("มีฝนตกเล็กน้อย"),
    trailing: IconButton(
      icon: Icon(Icons.cloudy_snowing),
      color: Colors.blue,
      onPressed: (){},
    ),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Text("25°", style: TextStyle(fontSize: 32)),
    title: Text("วันที่ 3 ม.ค. 2566"),
    subtitle: Text("มีเมฆมาก"),
    trailing: IconButton(
      icon: Icon(Icons.wb_cloudy),
      color: Colors.blue,
      onPressed: (){},
    ),
  );
}

AppBar buildAppBarWidget() {
  return AppBar(
    //backgroundColor: Colors.white,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.black,
    ),
    actions: <Widget>[
      IconButton(
        onPressed: () {},
        icon: Icon(Icons.star_border),
        color: Colors.black,
      ),
    ],
  );
}

Widget profileActionItems(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionButton(),
      buildPayButton(),
    ],
  );
}
